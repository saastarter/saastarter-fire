import { UserRepository } from "./users/user-repository";
import admin from "./firebase-admin";
import * as express from "express";
import * as firebaseAdmin from "firebase-admin";

/**
 * Express middleware that guaranties the user is authenticated.
 */


export interface AuthenticatedRequest extends express.Request {
  user: firebaseAdmin.auth.DecodedIdToken;
}

const getAuthToken = (
  req: express.Request,
  res: express.Response,
  next: express.NextFunction
) => {
  if (
    req.headers.authorization &&
    req.headers.authorization.split(" ")[0] === "Bearer"
  ) {
    (<any>req).authToken = req.headers.authorization.split(" ")[1];
  }
  next();
};

export const authenticationRequired = (
  req: express.Request,
  res: express.Response,
  next: any
) => {
  getAuthToken(req, res, async () => {
    try {
      const { authToken } = <any>req;
      const userInfo = await admin.auth().verifyIdToken(authToken);
      (<any>req).user = userInfo;

      return next();
    } catch (e) {
      return res
        .status(401)
        .send({ error: "You are not authorized to make this request" });
    }
  });
};

const userRepo = new UserRepository();

export const firestoreUserRequired = async (req: express.Request, res: express.Response, next: any) => {
  const authReq = req as AuthenticatedRequest;

  // Make sure the user object exists
  await userRepo.getOrCreate({
      uid: authReq.user.uid,
      email: authReq.user.email as string,
  })

  return next();
}