import * as functions from "firebase-functions";

export function getEnvVariable(name: string, defaultValue?:string|undefined) {
  const isEmulator = !!process.env.FUNCTIONS_EMULATOR;

  let value;
  if (isEmulator) {
    value = process.env[name];
  } else {
    const config = functions.config();
    if (!config.saastarter) {
      return undefined;
    }
    value = config.saastarter[name];
  }
  if (value === undefined) {
    return defaultValue;
  }
  return value;
}
