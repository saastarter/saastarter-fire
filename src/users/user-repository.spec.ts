import {
  createTestFirestore,
  testEmulatorSetup,
  testEmulatorTearDown
} from "../firestore";
import { UserRepository } from "./user-repository";

describe("User Repository", () => {
  // Make sure the emulator is setup and teared down properly
  beforeEach(async () => {
    await testEmulatorSetup();
    await createTestFirestore();
  });
  afterEach(async () => {
    await testEmulatorTearDown();
  });

  it("Create user", async () => {
    const repo = new UserRepository();
    const user = await repo.create({
      uid: "123456",
      email: "test@example.com"
    });
    expect(user.uid).toEqual("123456");
  });

  it("Get user", async () => {
    const repo = new UserRepository();
    expect(await repo.exists("123456")).toBeFalsy();

    await repo.create({
      uid: "123456",
      email: "test@example.com"
    });

    const user = await repo.get("123456");
    expect(user).not.toBeNull();
  });

  it("Does not exist", async () => {
    const repo = new UserRepository();
    expect(await repo.exists("123456")).toBeFalsy();
  });

  it("Update user", async () => {
    const repo = new UserRepository();
    await repo.create({
      uid: "123456",
      email: "test@example.com"
    });
    await repo.update({
      uid: "123456",
      email: "test2@example.com"
    });
    const updatedUser = await repo.get("123456");
    expect(updatedUser?.email).toEqual("test2@example.com");
  });
});
