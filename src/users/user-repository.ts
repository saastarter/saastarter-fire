import { getFirestore } from '../firestore';

export interface IUserData {
    uid: string,
    email: string,
    stripeCustomerId?: string
}
  

export class UserRepository {
    private db = getFirestore();

    public async exists(uid: string): Promise<boolean> {
        return !! await this.get(uid);
    }

    public async get(uid: string): Promise<IUserData|null> {
        const snap = await this.db.collection('users').doc(uid).get();
        if (!snap.exists) {
            return null;
        }
        return snap.data() as IUserData;
    }

    public async create(user: IUserData): Promise<IUserData> {
        if (await this.exists(user.uid)) {
            throw new Error('User already exists.');
        }
        await this.db.collection('users').doc(user.uid).set(user);
        return await this.get(user.uid) as IUserData;
    }
    
    public async getOrCreate(user: IUserData): Promise<IUserData> {
        const customer = await this.get(user.uid);
        if (customer) {
            return customer;
        } else {
            return await this.create(user);
        }
    }

    public async update(user: IUserData) {
        await this.db.collection('users').doc(user.uid).update(user);
    }
}

