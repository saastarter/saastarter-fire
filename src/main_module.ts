import * as dotenv from "dotenv";
dotenv.config();
/*tslint:disable:no-import-side-effect*/
import * as functions from "firebase-functions";

import {getEnvVariable} from "./env-var";

import billingApp from "./billing/billing-function";
import enableStripeEventPolling from "./billing/billing-function-dev";

const HOSTING_LOCATION = getEnvVariable('hosting_location', 'europe-west1');

export const billing = functions
  .region(HOSTING_LOCATION)
  .https.onRequest(billingApp);

if (process.env.FUNCTIONS_EMULATOR) {
  void enableStripeEventPolling();
}
