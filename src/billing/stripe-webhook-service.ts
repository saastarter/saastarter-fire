/*eslint max-classes-per-file: "error"*/
import Stripe from "stripe";
import { getFirestore } from "../firestore";
import * as express from "express";
import * as admin from "firebase-admin";
import { getEnvVariable } from "../env-var";

const stripe = new Stripe(getEnvVariable("stripe_api_key") as string, {
  apiVersion: "2020-08-27"
});
const WEBHOOK_SECRET = getEnvVariable("stripe_webhook_signing_secret");

export interface ISubscription {
  subscriptionId: string; // https://stripe.com/docs/api/subscriptions/object#subscription_object-id
  status: string; // https://stripe.com/docs/api/subscriptions/object#subscription_object-status

  priceId: string; // https://stripe.com/docs/api/prices/object#price_object-id
  currentPeriodStart: admin.firestore.Timestamp; // https://stripe.com/docs/api/subscriptions/object#subscription_object-current_period_start
  currentPeriodEnd: admin.firestore.Timestamp; // https://stripe.com/docs/api/subscriptions/object#subscription_object-current_period_end
  cancelAtPeriodEnd: boolean; // https://stripe.com/docs/api/subscriptions/object#subscription_object-status
  canceledAt: admin.firestore.Timestamp; // https://stripe.com/docs/api/subscriptions/object#subscription_object-canceled_at
  createdAt: admin.firestore.Timestamp; // https://stripe.com/docs/api/subscriptions/object#subscription_object-created
  updatedAt: admin.firestore.Timestamp;
}

export interface IOneTimePayment {
  paymentIntentId: string; // https://stripe.com/docs/api/payment_intents/object#payment_intent_object-id
  status: string; // https://stripe.com/docs/api/checkout/sessions/object#checkout_session_object-payment_status
  sessionId: string; // https://stripe.com/docs/api/checkout/sessions/object#checkout_session_object-id
  priceId: string; // https://stripe.com/docs/api/prices/object#price_object-id
  updatedAt: admin.firestore.Timestamp;
}

export class WebhookNotProcessableError extends Error {}

export class StripeWebhookService {
  private db = getFirestore();

  public verifyWebhook(req: express.Request) {
    const sig = req.headers["stripe-signature"];
    const body = (<any>req).rawBody;

    if (!WEBHOOK_SECRET) {
      throw new Error(
        "Webhook verification error: No Stripe signing secret provided."
      );
    }
    if (!sig) {
      throw new Error("Webhook verification error: No signature provided.");
    }
    try {
      const event = stripe.webhooks.constructEvent(body, sig, WEBHOOK_SECRET);
      return event;
    } catch (err) {
      throw new Error(`Webhook verification error: ${err.message}`);
    }
  }

  public async saveWebhook(event: any) {
    const type = <string>event.type;

    let processingSuccess: boolean = false;
    if (type.startsWith("customer.subscription.")) {
      processingSuccess = await this.processSubscriptionWebhook(event);
    } else if (type === "checkout.session.completed") {
      processingSuccess = await this.processCheckoutSessionWebhook(event);
    }
    if (processingSuccess) {
      await this.db
      .collection("stripe_webhooks")
      .doc(event.id)
      .set(event);
    }

  }

  private async processCheckoutSessionWebhook(event: any): Promise<boolean> {
    const paymentMode = event.data.object.mode;
    if (paymentMode !== "payment") {
      return true;
    }
    const customerId = event.data.object.customer;
    const snap = await this.db
      .collection("users")
      .where("stripeCustomerId", "==", customerId)
      .get();
    if (snap.docs.length !== 1) {
      return false;
      throw new WebhookNotProcessableError(
        "Processing webhook failed. Found none or several matching users. Event id: " +
          event.id
      );
    }

    const sessionId = event.data.object.id;
    const status = event.data.object.payment_status;
    const paymentIntentId = event.data.object.payment_intent;

    const session = await stripe.checkout.sessions.retrieve(sessionId, {
      expand: ["line_items"]
    });
    const priceId = session.line_items?.data[0].price.id as string;

    const user = snap.docs[0];

    const payment: IOneTimePayment = {
      paymentIntentId,
      status,
      sessionId,
      priceId,
      updatedAt: admin.firestore.Timestamp.fromMillis(event.created * 1000)
    };
    await this.db
      .collection("users")
      .doc(user.id)
      .collection("oneTimePayments")
      .doc(paymentIntentId)
      .set(payment);
      return true;
  }

  private async processSubscriptionWebhook(event: any) {
    const subscriptionId = event.data.object.id;
    const customerId = event.data.object.customer;
    const status = event.data.object.status;
    const createdAt = event.data.object.created;
    const canceledAt = event.data.object.canceled_at;
    const cancelAtPeriodEnd = event.data.object.cancel_at_period_end;
    const currentPeriodEnd = event.data.object.current_period_end;
    const currentPeriodStart = event.data.object.current_period_start;
    const priceId = event.data.object.items.data[0].price.id;

    const snap = await this.db
      .collection("users")
      .where("stripeCustomerId", "==", customerId)
      .get();
      if(snap.docs.length > 1) {
        return false;
        throw new Error(
          "Processing webhook failed. Found several matching users. Event id: " +
            event.id
        );
      } else if (snap.docs.length === 0) {
        return false;
      }

    const user = snap.docs[0];

    const subscription: ISubscription = {
      subscriptionId,
      status,
      cancelAtPeriodEnd,
      canceledAt,
      priceId,
      currentPeriodStart: admin.firestore.Timestamp.fromMillis(currentPeriodStart * 1000),
      currentPeriodEnd: admin.firestore.Timestamp.fromMillis(currentPeriodEnd * 1000),
      updatedAt: admin.firestore.Timestamp.fromMillis(event.created * 1000),
      createdAt
    };

    await this.db
      .collection("users")
      .doc(user.id)
      .collection("subscriptions")
      .doc(subscriptionId)
      .set(subscription);
    return true;
  }

  public async syncRecentWebhooks() {
    await this.syncWebhookType("customer.subscription.*");
    await this.syncWebhookType("checkout.session.completed");
  }

  private async syncWebhookType(type: string) {
    const toProcessWebhooks = [];
    for await (const event of stripe.events.list({
      type,
      limit: 1000
    })) {
      const snap = await this.db
        .collection("stripe_webhooks")
        .doc(event.id)
        .get();
      if (!snap.exists) {
        toProcessWebhooks.push(event);
      }
    }

    for (const event of toProcessWebhooks.reverse()) {
      try {
        await this.saveWebhook(event);
      } catch (e) {
        if (e instanceof WebhookNotProcessableError) {
          continue;
        } else {
          console.error("Error processing webhook", e);
        }
      }
    }
  }
}
