import { UserRepository, IUserData } from "../users/user-repository";
import Stripe from "stripe";
import {getEnvVariable} from '../env-var';


const stripe = new Stripe(getEnvVariable('stripe_api_key') as string, {
  apiVersion: "2020-08-27"
});


export class StripeService {
  private userRepository = new UserRepository();

  public async getOrCreateCustomerId(uid: string): Promise<string> {
    const user = await this.userRepository.get(uid);
    if (!user) {
      throw Error("User does not exist in FireStore. Create a user first");
    }
    if (user.stripeCustomerId) {
      return user.stripeCustomerId;
    }
    const customer = await this.createOrSyncStripeCustomer(user);
    return customer.id;
  }

  public async getOrCreateCustomer(uid: string): Promise<Stripe.Customer> {
    const user = await this.userRepository.get(uid);
    if (!user) {
      throw Error("User does not exist in FireStore. Create a user first");
    }
    if (user.stripeCustomerId) {
      const cust = await stripe.customers.retrieve(user.stripeCustomerId);
      if (cust.deleted) {
        throw new Error("Stripe customer has been deleted");
      }
      return cust as Stripe.Customer;
    }
    return await this.createOrSyncStripeCustomer(user);
  }

  private async createOrSyncStripeCustomer(
    user: IUserData
  ): Promise<Stripe.Customer> {
    const customerList = await stripe.customers.list({
      email: user.email
    });

    if (customerList.data.length > 1) {
      console.warn("Several customer for email " + user.email + " found.");
    }

    let customer: Stripe.Customer;
    if (customerList.data.length > 0) {
      customer = customerList.data[0];
    } else {
      customer = await stripe.customers.create({
        email: user.email,
        metadata: {
          firebaseUid: user.uid
        }
      });
    }
    user.stripeCustomerId = customer.id;
    await this.userRepository.update(user);
    return customer;
  }

  public async createCustomerPortalSession(uid: string, returnUrl: string): Promise<string> {
    const customerId = await this.getOrCreateCustomerId(uid);
    const session = await stripe.billingPortal.sessions.create({
      customer: customerId,
      return_url: returnUrl
    });

    return session.url;
  }

  public async createCheckoutSession(
    uid: string,
    priceId: string,
    successUrl: string,
    cancelUrl: string
  ) {

    let paymentType: 'subscription' | 'payment';
    const price = await stripe.prices.retrieve(priceId);
    if (price.type === 'recurring') {
      paymentType = 'subscription';
    } else if (price.type === 'one_time') {
      paymentType = 'payment';
    } else {
      throw new Error('Unknown price type');
    }

    const customerId = await this.getOrCreateCustomerId(uid);
    const session = await stripe.checkout.sessions.create({
      customer: customerId,
      payment_method_types: ["card"],
      line_items: [
        {
          price: priceId,
          quantity: 1
        }
      ],
      mode: paymentType,
      success_url: successUrl,
      cancel_url: cancelUrl
    });
    return session;
  }

}
