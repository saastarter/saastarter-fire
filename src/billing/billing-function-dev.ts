import { StripeWebhookService } from "./stripe-webhook-service";

const webhookService = new StripeWebhookService();

/*
  During development, it is hard to receive Stripe webhooks and therefore sync Stripe subscriptions
  with the Firebase emulator. This module provides a function to sync all the events from Stripe as good as
  possible.
  Alternatively, one can use https://ngrok.com/ to use Stripe webhooks directly.
*/

/**
 * Enables polling of Stripe events. Replaces webhooks during development.
 * @param pollInterval Stripe events poll interval in ms
 */
export default async function enableStripeEventPolling(pollInterval = 10000) {
  console.log(
    `[Dev only] Schedule Stripe webhook sync every ${pollInterval /
      1000} seconds.`
  );
  void continuallyPoll(pollInterval);
}

async function continuallyPoll(pollInterval: number) {
  while (true) {
    // const start = new Date();
    // console.debug("- Sync Stripe events", start);
    try {
      await webhookService.syncRecentWebhooks();
    } catch (e) {
      console.error('Error polling Stripe webhooks', e);
    }
    
    // const duration = new Date().getTime() - start.getTime();
    // console.debug("  - Took", duration / 1000, "seconds");
    await sleep(pollInterval);
  }
}

async function sleep(miliseconds: number) {
  await new Promise(resolve => {
    setTimeout(() => {
      resolve();
    }, miliseconds);
  });
}
