import * as express from "express";
import * as cors from "cors";
import * as bodyParser from "body-parser";
import {
  authenticationRequired,
  AuthenticatedRequest,
  firestoreUserRequired
} from "../firebase-auth";
import { StripeService } from "./stripe-service";
import { StripeWebhookService, WebhookNotProcessableError } from "./stripe-webhook-service";
import { body } from 'express-validator';

const stripeService = new StripeService();
const stripeWebhookService = new StripeWebhookService();

const app = express();
app.use(cors({}));

app.post(
  "/create-customer-portal-session",
  authenticationRequired,
  firestoreUserRequired,
  async (req, res) => {
    const authReq = req as AuthenticatedRequest;
    const userId = authReq.user.uid;
    const returnUrl = req.body.data.returnUrl;

    const url = await stripeService.createCustomerPortalSession(
      userId,
      returnUrl
    );
    res.status(200).json({
      data: url
    });
  }
);

app.use(
  "/webhooks/stripe",
  bodyParser.json({
    // Because Stripe needs the raw body, we compute it but only when hitting the Stripe callback URL.
    verify(req, res, buf) {
      (<any>req).rawBody = buf.toString();
    }
  })
);

app.post("/stripe-webhook", async (req, res) => {
  try {
    const event = stripeWebhookService.verifyWebhook(req);
    await stripeWebhookService.saveWebhook(event);

    res.status(200).send("ok");
  } catch (e) {
    if (e instanceof WebhookNotProcessableError) {
      res.status(200).send("Do not process this specific hook.");
      return;
    }
    console.error(e);
    res.status(400).send("Verification error:" + e);
    return;
  }
});

app.post(
  "/create-checkout-session",
  [
    body('userId').isString().not().isEmpty(),
    body('priceId').isString().not().isEmpty(),
    body('successUrl').isURL().not().isEmpty(),
    body('cancelUrl').isURL().not().isEmpty(),
  ],
  authenticationRequired,
  firestoreUserRequired,
  async (req: any, res: any) => {
    const authReq = req as AuthenticatedRequest;
    const userId = authReq.user.uid;
    const priceId = req.body.data.priceId;
    const successUrl = req.body.data.successUrl;
    const cancelUrl = req.body.data.cancelUrl;

    try {
      const session = await stripeService.createCheckoutSession(
        userId,
        priceId,
        successUrl,
        cancelUrl
      );
      res.status(200).json({
        data: session.id
      });
    } catch (e) {
      console.error(e);
      if (e.param === 'price' && e.code === 'resource_missing') {
        res.status(400).json({
          error: {
            message: 'Stripe plan does not exist',
            details: e
          }
        });
      } else {
        res.status(400).json({
          error: {
            message: 'Unkown error',
            details: e
          }
        });
      }

      return;
    }
  }
);

export default app;
