import {createTestFirestore, testEmulatorSetup, testEmulatorTearDown} from '../firestore';
import {UserRepository} from '../users/user-repository';


xdescribe('Stripe Service', () => {

    // Make sure the emulator is setup and teared down properly
    beforeEach(async () => {
      await testEmulatorSetup();
      await createTestFirestore();
    });
    afterEach(async () => {
      await testEmulatorTearDown();;
    });

    it("Create customer", async () => {
      const repo = new UserRepository();
      const user = await repo.create({uid: '123456', email: 'test@example.com'});
      expect(user.uid).toEqual('123456');
    });


});