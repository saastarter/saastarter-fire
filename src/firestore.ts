import * as admin from "firebase-admin";
import app from './firebase-admin';
import * as firebase from "@firebase/testing";

let db: admin.firestore.Firestore;

if (process.env.NODE_ENV !== "test") {
  db = app.firestore();
}

export function getFirestore(): admin.firestore.Firestore {
  return db;
};

export const createTestFirestore = async (loadRules=false, auth: any=undefined, projectId="test-project") => {
    const appl = firebase.initializeTestApp({ projectId, auth });
    db = appl.firestore() as unknown as admin.firestore.Firestore;
    if (!loadRules) {
      await firebase.loadFirestoreRules({
        projectId,
        rules: noRules
      });
    }
    return db;
};

export const testEmulatorSetup = async (projectId="test-project") => {
    await firebase.clearFirestoreData({ projectId });
}

export const testEmulatorTearDown = async () => {
    await Promise.all(firebase.apps().map(appl => appl.delete()));
}


const noRules = 
`
rules_version = '2';
service cloud.firestore {
  match /databases/{database}/documents {
    match /{document=**} {
      allow read, write: if true;
    }
  }
}
`;