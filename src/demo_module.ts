import * as functions from "firebase-functions";
import * as express from "express";
import * as cors from "cors";
import {
  authenticationRequired,
  AuthenticatedRequest
} from "./firebase-auth";
import {getEnvVariable} from "./env-var";

/**
 * This is an example implementation of a Firebase function server.
 * Feel free to remove the content in this file if you don't like it.
 */

/**
 * Define the express server and use the CORS middleware.
 */
const app = express();
app.use(cors({}));

const HOSTING_LOCATION = getEnvVariable('hosting_location', 'europe-west1'); // Defines where the function are hosted.

/**
 * Unauthenticated request
 */
app.post("/", async (req, res) => {
  res.status(200).json({
    data: "Hello World! 👋"
  });
});

/**
 * Authenticated request
 */
app.post("/auth-required", authenticationRequired, async (req, res) => {
  const authReq = req as AuthenticatedRequest;
  res.status(200).json({
    data: "Hello " + authReq.user.email + "! 👋"
  });
});

export const helloWorld = functions
  .region(HOSTING_LOCATION)
  .https.onRequest(app);

