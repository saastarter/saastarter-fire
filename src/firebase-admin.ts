import * as admin from 'firebase-admin';

function intializeApp(): admin.app.App {
    if (admin.apps.length > 0) {
        return admin.apps[0] as admin.app.App;
    }
    return admin.initializeApp();
}
const app = intializeApp();
export default app;

